import React from 'react';



/*REACT Bootstrap Stuff*/
import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';

//My CSS stuff
import "./css/Math_Talk_Regarding_Sets.css"
import "./css/General_Blog.css"

class Math_Talk_Regarding_Sets extends React.Component {
    render(){
        return(
            <Grid>
                <Row>
                    <Col md={12}>
                        <h1 className="Blog-Headers"> "Math Talk Regarding Sets" </h1>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <p className="Blog-Text">
                            Any serious book on mathematics begins with an explanation of sets.
                            There's a reason for this, as sets provide a descriptive understanding of "the world around a problem."
                            Certain approaches and formulas only apply to certain contexts.  Imagine that you're working on fixing a car.
                            You wouldn't necessarily bring a spatula, because that tool wouldn't do anything to the nuts and bolts of a car.
                        </p>
                        <p className="Blog-Text">
                            A basic vocabulary is needed for describing things precisely, and as such, I thought I might explore set theory a bit below.
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What is a set?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <p className="Blog-Text">
                            A set is a collection of <em>distinct</em> objects or elements.  Note the word "distinct," as two individual objects 
                            may not be in the same set. 
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What is an empty set?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <p className="Blog-Text">
                            An empty set is a set consisting of no elements at all.
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What are some ways of communicating sets?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <p className="Blog-Text">
                            If you wanted to communicate finite sets, you might say something like...
                        </p>#
                        <p className="Blog-Text">
                            Let A be a finite set defined as &#123; 1,2,3,4 &#125; 
                            <br/>
                            Or, alternatively
                            Let A be defined as &#123; x | 0 &#60; x &#60;= 4 and x is an element of the set of integers &#125;
                        </p>
                        <p className="Blog-Text">
                            If you wanted to communicate infinite sets, you might say something like...
                        </p>
                        <p className="Blog-Text">
                            Let A be a finite set defined as &#123; 1,2,3,4,... &#125;
                            <br/>
                            Or, alternatively
                            Let A be defined as &#123; x | x &#62; 0 and x &#8712; Z &#125;
                        </p>
                        <p className="Blog-Text">
                            Where the vertical bar there means "such that".
                        </p>
                        <p className="Blog-Text">
                            Where &#8712; means "element of".
                        </p>
                        <p className="Blog-Text">
                            Where Z means "the set of all integers".
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What does it mean to say one set is a subset of another?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <p className="Blog-Text">
                            A subset is simply a set that is contained in another set.  That is, all the elements of a subset also belong
                            to another set.
                        </p>
                        <p className="Blog-Text">
                            Syntactically, one might say something like F &#8834; S, where F is the set of all Female students and S is the set of all students.
                        </p>
                        <p className="Blog-Text">
                            The symbole &#8834; means "subset of"
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What a proper subset?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <p className="Blog-Text">
                            Imagine you had two sets.  Let one set be defined as all the students in a class, 'S'.
                            Let another set be defined as all the female students in a class, 'F'.
                        </p>
                        <p className="Blog-Text">
                            Theoretically, you can have a class consisting of all female students, but yet F &#8834; S. 
                            This leads to some amount of ambiguity in defining set relations, as F is essentially equivalent to S.
                        </p>
                        <p className="Blog-Text">
                            How does one say that F &#8834; S, but there may be some other elements that are not in F? That is, how does one 
                            imply that there may be non-female students in a class?
                        </p>
                        <p className="Blog-Text">
                            By defining F as a "proper subset".  That is, F is a subset of S, but not equivalent to S.  
                        </p>
                        <p className="Blog-Text">
                            Pictorally, this might look like...
                        </p>
                        <div className="f_proper_subset_s_image"/>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What is an union between two sets?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>                 
                        <p className="Blog-Text">
                            Imagine that you have three sets A, B, C.
                            <br/>
                            Let A &#8746; B &#8746; C.
                            <br/>
                            Where &#8746; means "union"
                            <br/>
                            The set of all elements of A, B, C, belonging to <em>at least</em> on of the sets, is considered
                            the <em>union</em> of sets A, B, C.
                        </p>
                        <p className="Blog-Text">
                            Pictorially, this might look like...
                        </p>
                        <div className="a_un_b_un_c_image"/>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <h3 className="Blog-Headers"> "What is an intersection between two sets?" </h3>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>                 
                        <p className="Blog-Text">
                            Imagine that you have three sets A, B, C.
                            <br/>
                            Let A &#8745; B &#8745; C.
                            <br/>
                            Where &#8745; mean "intersection"
                            <br/>
                            The set of all elements of A, B, C, belonging to <em>all</em> of those sets is considered the <em>intersection</em> of sets A, B, C
                        </p>
                        <p className="Blog-Text">
                            Pictorially, this might look like...
                        </p>
                        <div className="a_int_b_int_c_image"/>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

export default Math_Talk_Regarding_Sets;