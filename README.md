## Creating a blog entry
1. Create meta information in src/config/blogs.json
   Note: The name must exactly match the name of the react component (sadly)
2. Create content in src/blogs/
3. Update src/Blog.js with relevant import

## Creating a project entry
1. Create meta information in src/config/projects.json
   Note: The name must exactly match the name of the react component (sadly)
2. Create content in src/projects/
3. Update src/Project.js with relevant import

## Running the code locally
1. After a git clone, make sure you do a `npm install`
2. To run do `npm start`

## Building and deploying the code
IMPORTANT: There's a problem regarding query-string in react-script builds.
Open node_modules/react-scripts/webpack.config.prod.js.
and alter devtool to be `cheap-module-eval-source-map`

Then do npm run-script build.  Copy the contents of the build directory to s3 bucket.
S3 bucket is assumed to be configured for static hosting.